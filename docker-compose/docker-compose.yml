version: '3.7'

volumes:
    certs: {}
    caddy_config: {}
    caddy_data: {}
    cells_logs: {}
    cells_metrics: {}
    cells_working_dir: {}
    cells_db: {}
    mm_data: {}
    mm_db: {}
    prom_data: {}

services:
    # Caddy v2 reverse proxy with Let's Encrypt certificates  
    reverse:
        image:  caddy:2-alpine
        ports: ["${PROXY_PORT}:443"]
        restart: unless-stopped
        volumes:
            - ./conf/Caddyfile:/etc/caddy/Caddyfile
            - certs:/var/certs
            - cells_logs:/var/www/logs
            - caddy_data:/data
            - caddy_config:/config
        environment:
            - CADDYPATH=/var/certs
            - PUBLIC_FQDN=${PUBLIC_FQDN}
            - ADMIN_PWD=${SYSADMIN_PASSWORD}

    # Generate self signed certificates for both Caddy and Cells
    certs:
        image:  bsinou/mkcert:latest
        volumes:
            - certs:/var/certs
        environment:
            - DOMAINS=${PUBLIC_FQDN},cells

    cells_db:
        image: mysql:8.0.21
        volumes:
            - cells_db:/var/lib/mysql
        environment:
            - MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PWD}
            - MYSQL_DATABASE=cells_${BOX_NAME}
            - MYSQL_USER=${MYSQL_APP_USER_LOGIN}
            - MYSQL_PASSWORD=${MYSQL_APP_USER_PWD}
        cap_add:
            - SYS_NICE
        command: [mysqld, --character-set-server=utf8mb4, --collation-server=utf8mb4_unicode_ci, --default_authentication_plugin=mysql_native_password]

    mm_db:
        image: mysql:8.0.21
        restart: unless-stopped
        volumes:
            - mm_db:/var/lib/mysql
        environment:
            - MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PWD}
            - MYSQL_DATABASE=mm_${BOX_NAME}
            - MYSQL_USER=${MYSQL_APP_USER_LOGIN}
            - MYSQL_PASSWORD=${MYSQL_APP_USER_PWD}
        cap_add:
            - SYS_NICE
        command: [mysqld, --character-set-server=utf8mb4, --collation-server=utf8mb4_unicode_ci, --default_authentication_plugin=mysql_native_password]

    cells:
        image: ${CELLS_DOCKER_IMAGE}
        restart: unless-stopped
        expose: [8080]
        volumes: 
            - ${VAR_DIR}/${BOX_NAME}/bin:/usr/local/bin
            - cells_logs:/var/cells/logs
            - cells_working_dir:/var/cells
            - cells_metrics/var/cells/services/pydio.gateway.metrics/
            - ./conf/pydio-license:/var/cells/pydio-license:ro
            - ./conf/install-conf.yaml:/var/cells/install-conf.yaml:ro
        environment:
            - CELLS_INSTALL_YAML=/var/cells/install-conf.yaml
            - CELLS_BIND=${PUBLIC_FQDN}:8080
            - CELLS_EXTERNAL=https://${PUBLIC_FQDN}
            - CELLS_ADMIN_PASSWORD=${CELLS_ADMIN_PASSWORD}
            - MYSQL_DB_NAME=cells_${BOX_NAME}
            - MYSQL_USER_LOGIN=${MYSQL_APP_USER_LOGIN}
            - MYSQL_USER_PWD=${MYSQL_APP_USER_PWD}
            - CELLS_LOGS_LEVEL=production
            - CELLS_ENABLE_METRICS=true
        depends_on:
            - cells_db
    
    mattermost:
        image: ${MM_DOCKER_IMAGE}
        restart: unless-stopped
        expose: [8000]
        volumes:
            - mm_data:/mattermost:rw
            - /etc/localtime:/etc/localtime:ro
        environment:
            - MM_USERNAME=${MYSQL_APP_USER_LOGIN}
            - MM_PASSWORD=${MYSQL_APP_USER_PWD}
            - MM_DBNAME=mm_${BOX_NAME}
            - MM_SERVICESETTINGS_SITEURL=https://${PUBLIC_FQDN}/chat
            - DB_HOST=mm_db
            - DB_PORT_NUMBER=3306
            - MM_SQLSETTINGS_DRIVERNAME=mysql
            - MM_SQLSETTINGS_DATASOURCE=${MYSQL_APP_USER_LOGIN}:${MYSQL_APP_USER_PWD}@tcp(mm_db:3306)/mm_${BOX_NAME}?charset=utf8mb4,utf8&readTimeout=30s&writeTimeout=30s
        depends_on:
            - mm_db

    # Prometheus to expose metrics
    prometheus:
        image: prom/prometheus
        restart: unless-stopped
        expose: [9090]
        volumes: 
            - prom_data:/prometheus       
            - ./conf/prometheus.yml:/etc/prometheus/prometheus.yml
            - cells_metrics:/var/cells
        command: 
            - --config.file=/etc/prometheus/prometheus.yml
            - --storage.tsdb.path=/prometheus 
            - --storage.tsdb.retention.time=30d 
            - --web.external-url=https://${PUBLIC_FQDN}/prometheus

    cadvisor:
        image: gcr.io/google-containers/cadvisor:latest
        expose: [8080]
        volumes:
            - /:/rootfs:ro
            - /var/run:/var/run:rw
            - /sys:/sys:ro
            - /var/lib/docker/:/var/lib/docker:ro
        command:
            - --url_base_prefix=/cadvisor
