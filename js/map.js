// Hardcoded links
var data = [
  {
    name: 'Stratford, UK',
    latLng: [51.5458181, -0.0183608],
    id: 'stratford',
    description: 'Introduction',
    link: 'https://vimeo.com/312142602'
  },
  {
    name: 'Austria',
    latLng: [47.1009653, 15.7116181],
    id: 'maihan',
    description: 'Dream of a refugee',
    link: 'https://vimeo.com/moshaprod/maihan'
  },
  {
    name: 'Mikolow, Poland',
    latLng: [50.1790184, 18.9038037],
    id: 'mikolow',
    description: 'Going to Auschwitz',
    link: 'https://vimeo.com/314545476/93edd29b1f'
  }
];

// Create the map
var map = new L.map('map', {
  center: new L.LatLng(49.201640, 10.169361),
  zoom: 6,
  maxZoom: 18,
  layers: new L.TileLayer('https://{s}.tiles.mapbox.com/v3/gvenech.m13knc8e/{z}/{x}/{y}.png')
});

// Add a place to save markers
var markers = {};

// Loop through the data
for (var i = 0; i < data.length; i++) {
  var place = data[i];

  // Create and save a reference to each marker
  markers[place.id] = L.marker(place.latLng, {
    //icon: ...,
    //draggable: ...,
    title: place.name,
    alt: place.description,
    //zIndexOffset:	0,
    //opacity: 1,
    riseOnHover: true
    //riseOffset: 250
  }).addTo(map);

  // Trick to pass the id to the leaflet marker
  markers[place.id]._icon.id = place.id
}

// Open a video popup when clicking on a marcker
$('.leaflet-marker-icon').on('click', function (e) {
  // Use the event to find the clicked element
  var el = $(e.srcElement || e.target),
  
  id = el.attr('id');
  currPlace = data.find(x => x.id === id);

  console.log(currPlace.description)

  $.magnificPopup.open({
    items: {
      src: currPlace.link
    },
    disableOn: 449,
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,
    fixedContentPos: false
  });

  // One way you could use the id
  // map.panTo(markers[id].getLatLng());
});
